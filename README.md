# ToDoList
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.1.

  - this project is a to-do list for a [GUEP](https://www.guep.com.br/) test

<h1>Preview </h1>
 <img id="Preview" src="./.bitbucket/ToDoPreview.png" >

 <img id="Preview" src="./.bitbucket/ToDoPreview1.png" >

 <h3> The design is fully responsive </h3>

<h1> This project was developed with the following technologies: </h1>
### Angular
Angular is a platform for building mobile and desktop web applications.

Website: https://angular.io/

### Typescript 
TypeScript is an open-source language which builds on JavaScript, one of the world’s most used tools, by adding static type definitions. (Official Website)

Website: https://www.typescriptlang.org/

### HTML
HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content. 

Website: https://developer.mozilla.org/en-US/docs/Web/HTML
### CSS
Cascading Style Sheets (CSS) is a stylesheet language used to describe the presentation of a document written in HTML or XML (including XML dialects such as SVG, MathML or XHTML). CSS describes how elements should be rendered on screen, on paper, in speech, or on other media.

Website: https://developer.mozilla.org/en-US/docs/Web/CSS
 

# Running Locally
First of all, clone the repository by running the following command:

```bash
git clone https://Mateus-Quintino@bitbucket.org/mateus-quintino/todo-list.git
```
### Installing the project's dependencies

You can install the project's dependencies by running the following command inside the project's folder:

```bash
npm install
```

## Open project directly in browser
You can open the project directly in browser with the following command:

```bash
ng serve --open
```


## If you want to add something in the project
### Code scaffolding
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

# Flowchart

<img id="Preview" src="./.bitbucket/ToDoFlowchart.png" >
