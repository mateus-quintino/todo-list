import { Component, OnInit } from '@angular/core';
import { Todo } from '../../models/Todo';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css'],
})
export class TodosComponent implements OnInit {
  todos: Todo[];

  inputTodo = '';

  constructor() {}

  ngOnInit(): void {
    const storagedToDos = localStorage.getItem('tasks');
    storagedToDos
      ? (this.todos = JSON.parse(storagedToDos))
      : (this.todos = []);
  }

  createTask() {
    this.todos.push({
      content: this.inputTodo,
      completed: false,
    });
    const storagedToDos = localStorage.getItem('tasks');
    if (storagedToDos) {
      const parsedStorageToDos = JSON.parse(storagedToDos);
      localStorage.setItem(
        'tasks',
        JSON.stringify([
          ...parsedStorageToDos,
          { content: this.inputTodo, completed: false },
        ])
      );
    } else {
      localStorage.setItem(
        'tasks',
        JSON.stringify([{ content: this.inputTodo, completed: false }])
      );
    }
    this.inputTodo = '';
  }

  taskDone(id: number) {
    this.todos.map((v, i) => {
      if (i == id) v.completed = !v.completed;
      return v;
    });
    const storagedToDos = localStorage.getItem('tasks');
    if (storagedToDos) {
      localStorage.setItem('tasks', JSON.stringify(this.todos));
    }
  }

  deleteTask(id: number) {
    this.todos = this.todos.filter((v, i) => i != id);
    const storagedToDos = localStorage.getItem('tasks');
    if (storagedToDos) {
      localStorage.setItem('tasks', JSON.stringify(this.todos));
    }
  }
}
